package businessLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTable;

import dataAccess.CustomerDao;
import dataAccess.OrderDao;
import dataAccess.ProductDao;
import model.Comparators;
import model.Customer;
import model.Orders;
import model.Product;
import model.SortingFilters;
import presentation.BillWindow;
import presentation.ClientsWindow;
import presentation.MenuWindow;
import presentation.OrderWindow;
import presentation.ProductWindow;
/**
 * This class is used to control the GUI and the Model.
 * @author Rares
 *
 */
public class Controller {
	private MenuWindow menu; 
	private ClientsWindow clients; 
	private ProductWindow product; 
	private OrderWindow order;  
	private BillWindow bill;
	
	@SuppressWarnings("deprecation")
	public Controller () { 
		
		CustomerDao c = new CustomerDao(); 
		OrderDao o = new OrderDao(); 
		
		List<Customer> customers = c.findAll(); 
		List<Orders> orders = o.findAll(); 
		List<Object> objectCustomer = new ArrayList<Object>(customers); 
		List<Object> objectOrder = new ArrayList<Object>(orders);
		
		JTable customerTable = c.createTable(objectCustomer); 
		JTable orderTable = o.createTable(objectOrder);
		
		
		this.menu = new MenuWindow();  
		this.menu.setVisible(true);
		this.clients= new ClientsWindow(customerTable,this.menu);  
		this.clients.hide();
		this.product = null;
		this.order = null; 
		this.bill = new BillWindow(orderTable, this.menu); 
		this.bill.hide();
		this.menu.addClientsListener(new OpenClientList()); 
		this.menu.addProductsListener(new OpenProductList()); 
		this.menu.addOrderListener(new OpenOrderListener()); 
		this.menu.addBillListener(new BillingListener());
		//this.clients.goBack(new ClientsBack()); 
		//this.product.goBack(new ProductBack()); 
		//this.order.goBack(new OrderBack()); 
		this.menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		this.clients.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		this.bill.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	} 
	
	class OpenClientList implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			Controller.this.menu.hide(); 
			Controller.this.clients.show(); 
		}
		
	} 
	
	class OpenProductList implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) { 
			ProductDao p = new ProductDao();  
			List<Product> products = p.findAll();  
			SortingFilters filters; 
			if(Controller.this.menu.getState() == 1) 
				filters = SortingFilters.BY_PRICE; 
			else {
				if(Controller.this.menu.getState() == 2) 
					filters = SortingFilters.BY_CATEGORY; 
				else 
					filters = SortingFilters.BY_RATINGS; 
			} 
			Collections.sort(products, new Comparators(filters));
			List<Object> objectProduct = new ArrayList<Object>(products); 
			JTable productTable = p.createTable(objectProduct);  
			Controller.this.product = new ProductWindow(productTable, Controller.this.menu);
			Controller.this.menu.hide(); 
			Controller.this.product.show();  
			Controller.this.product.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		}
		
	} 
	
	class OpenOrderListener implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) { 
			
			OrderDao o = new OrderDao(); 
			List<Orders> orders = o.findAll(); 
			List<Object> objectOrder = new ArrayList<Object>(orders);
			JTable orderTable = o.createTable(objectOrder);
			Controller.this.order = new OrderWindow(orderTable, Controller.this.menu);
			Controller.this.order.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
			Controller.this.menu.hide(); 
			Controller.this.order.show(); 
		}
		
	}  
	
	class BillingListener implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			Controller.this.menu.hide(); 
			Controller.this.bill.show(); 
		}
		
	} 
	
	class ClientsBack implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			Controller.this.clients.hide(); 
			Controller.this.menu.show();
		}
		
	} 
	
	class ProductBack implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			Controller.this.product.hide(); 
			Controller.this.menu.show();
		}
		
	}
	
	class OrderBack implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			Controller.this.order.hide(); 
			Controller.this.menu.show();
		}
		
	} 
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Controller controller = new Controller();

	}
}
