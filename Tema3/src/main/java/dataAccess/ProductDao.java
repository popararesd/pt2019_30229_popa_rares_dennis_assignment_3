package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Product;
/**
 * This class implements unique methods for the Product entity.
 * @author Rares
 *
 */
public class ProductDao extends AbstractDao<Product> {
	/**
	 * This method is used to insert a new Product in the database.
	 * @param p The product to be inserted in the database.
	 * @return A boolean true for success , false for failure.
	 */
	public boolean insertProduct(Product p ) {
		String query = "INSERT INTO product (name , category , price , ratings , stock ) VALUES ( ? , ? , ? , ? , ?)"; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			statement.setString(1, p.getName()); 
			statement.setLong(2, p.getCategory()); 
			statement.setLong(3, p.getPrice()); 
			statement.setLong(4, p.getRatings()); 
			statement.setLong(5, p.getStock());
			statement.executeUpdate(); 
			return true;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return false;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	} 
	/**
	 * This method updates the entry based on the id.
	 * @param id The id of the entry to be modified.
	 * @param name The new name.
	 * @param category  The new category.
	 * @param price The new price.
	 * @param ratings The new ratings.
	 * @param stock The new stock.
	 * @return A boolean true for success , false for failure.
	 */
	public boolean updateProduct (int id , String name , int category , int price , int ratings , int stock) {
		String query = "UPDATE product SET name=?, category=?, price=?, ratings=?, stock=? WHERE id="+id; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			statement.setString(1, name); 
			statement.setLong(2, category); 
			statement.setLong(3, price);  
			statement.setLong(4, ratings); 
			statement.setLong(5, stock);
			statement.executeUpdate(); 
			return true;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return false;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	} 
	/**
	 * This method returns the price of the product with the given id.
	 * @param id The id of the product.
	 * @return The price of the product.
	 */
	public int getPriceById(int id) {
		String query = "Select price FROM product WHERE id="+id; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		ResultSet rs = null;
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			rs = statement.executeQuery();  
			rs.next();
			Integer out = (Integer) rs.getObject("price");
			return out;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return -1;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	} 
	/**
	 * This method returns the stock of the product with the given id.
	 * @param id The id of the product.
	 * @return The stock of the product.
	 */
	public int getStockById(int id) {
		String query = "Select stock FROM product WHERE id="+id; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		ResultSet rs = null;
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			rs = statement.executeQuery();  
			rs.next();
			Integer out = (Integer) rs.getObject("stock");
			return out;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return -1;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	} 
	/**
	 * This method updates the stock of the product with the given id.
	 * @param id The id of the product.
	 * @return A boolean true for success , false for failure.
	 */
	public  boolean updateStock(int id , int stock){
	String query = "UPDATE product SET  stock=? WHERE id="+id; 
	Connection connection = null; 
	PreparedStatement statement = null;  
	try {
		connection = ConnectionFactory.getConnection(); 
		statement = connection.prepareStatement(query);
		statement.setLong(1, stock);
		statement.executeUpdate(); 
		return true;
	} 
	catch(Exception ex) { 
		System.out.println(ex.getMessage());
		return false;
	} 
	finally {
		ConnectionFactory.close(connection); 
		ConnectionFactory.close(statement);
	} 
} 
	
}
