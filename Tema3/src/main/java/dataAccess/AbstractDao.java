package dataAccess;

import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
/** 
 * This class is used to access the database for basic modifications and basic selections 
 * for the entries in the database
 * @author Rares
 *
 * @param <T> Represents the type / entity for the operations 
 */
public abstract class AbstractDao<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDao.class.getName()); 
	private final Class<T> type; 
	
	@SuppressWarnings("unchecked") 
	/** 
	 * This constructor creates a new instance of the class. 
	 * The attribute type is being initialized as a basic entity type.
	 */
	public AbstractDao () {
		this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0]; 
		
	} 
	
	/**
	 * This method returns a String containg a query based on the parameter.
	 * @param field A String that represents the name of the field from the entity
	 * @return A String containing the query. 
	 */
	private String createSelectQuery(String field) { 
		if(field.equals("")) {
			return "SELECT * FROM " + type.getSimpleName().toLowerCase();
		}
		StringBuilder str = new StringBuilder(); 
		str.append("SELECT "); 
		str.append(" * "); 
		str.append(" FROM "); 
		str.append(type.getSimpleName().toLowerCase()); 
		str.append(" WHERE " + field + " =?"); 
		return str.toString();
	}  
	/**
	 * This method creates a Delete query based on the value of field
	 * @param field The name of the field from the entity
	 * @return The Delete Query based on the field
	 */
	private String createDeleteStatement(String field) {
		StringBuilder str = new StringBuilder(); 
		str.append("DELETE ");  
		str.append(" FROM "); 
		str.append(type.getSimpleName().toLowerCase()); 
		str.append(" WHERE " + field + " =?"); 
		return str.toString();
	}
	
	@SuppressWarnings("deprecation") 
	/**
	 * This method returns a List containing the elements from the ResultSet.
	 * @param rs The ResultSet to be converted.
	 * @return A list containing all entries from the ResultSet.
	 */
	private List<T> getResults(ResultSet rs){
		List<T> output = new ArrayList<T>();  
		try { 
			while(rs.next()) { 
				T instance = type.newInstance();  
				for(Field field : type.getDeclaredFields()) {
					Object value = rs.getObject(field.getName()); 
					PropertyDescriptor propDesc = new PropertyDescriptor(field.getName(), type); 
					Method method = propDesc.getWriteMethod(); 
					method.invoke(instance, value);
				} 
				output.add(instance); 
			} 
			return output;
		} 
		catch(Exception ex) {
			
		} 
		return null;
	}  
	
	/** 
	 * This method creates a JTable based on the list of objects provided.
	 * @param objects The list of Objects to be converted. 
	 * @return A JTable containing the entries in the List
	 */
	
	public JTable createTable(List<Object> objects) {
		
		int nrOfFields = 0; 
		
		for(@SuppressWarnings("unused") Field field : objects.get(0).getClass().getDeclaredFields()) 
			nrOfFields++;
		
		Object[] tableHeader = new Object[nrOfFields]; 
		Object[][]tableData = new Object[objects.size()][nrOfFields]; 
		Object obj = objects.get(0);  
		int i=0; 
		System.out.println("Here");
		for(Field field :  obj.getClass().getDeclaredFields()) {
			tableHeader[i] = field.getName().toUpperCase();  
			System.out.println(field.getName());
			i++;
		}  
		System.out.println("Here");
		for(int k=0;k<objects.size();k++) {
			Object o = objects.get(k);  
			int a = 0; 
			try {
			for(Field field : o.getClass().getDeclaredFields()) {
				PropertyDescriptor propDesc = new PropertyDescriptor(field.getName(), o.getClass());
				Method getter = propDesc.getReadMethod(); 
				Object value = getter.invoke(o); 
				tableData[k][a]=value; 
				a++;
			} 
			} 
			catch(Exception ex) {
				System.out.println(ex.getMessage());
			}
		} 
		
		DefaultTableModel model = new DefaultTableModel(); 
		model.setDataVector(tableData, tableHeader); 
		JTable table = new JTable(model); 
		
		return table;
		
	}
	/**
	 * This method returns all entries in the table from the database.
	 * @return A List containing all entries in the database
	 */
	public List<T> findAll(){
		Connection con = null; 
		PreparedStatement statement = null; 
		ResultSet rs = null; 
		String query = createSelectQuery("");  
		try {
			con = ConnectionFactory.getConnection(); 
			statement = con.prepareStatement(query); 
			rs = statement.executeQuery(); 
			return getResults(rs);
		} 
		catch(Exception ex) {
			
		} 
		finally {
			ConnectionFactory.close(con); 
			ConnectionFactory.close(rs); 
			ConnectionFactory.close(statement);
		} 
		return null;
	}   
	/**
	 * This method returns all entries in the table that match the id sent as parameter. 
	 * @param id The id of the entry.
	 * @return A List containing the entries in the database
	 */
	public List<T> findById(int id){
		Connection con = null; 
		PreparedStatement statement = null; 
		ResultSet rs = null; 
		String query = createSelectQuery("id");  
		try {
			con = ConnectionFactory.getConnection(); 
			statement = con.prepareStatement(query);  
			statement.setInt(1, id);
			rs = statement.executeQuery(); 
			return getResults(rs);
		} 
		catch(Exception ex) {
			
		} 
		finally {
			ConnectionFactory.close(con); 
			ConnectionFactory.close(rs); 
			ConnectionFactory.close(statement);
		} 
		return null;
	} 
	
	/**
	 * This method deletes all entries in the table that match the id sent as parameter. 
	 * @param id The id of the entry. 
	 * @return A boolean value : true for success and fasle for failure.
	 */
	public boolean deleteById(int id) {
		Connection con = null; 
		PreparedStatement statement = null;  
		String query = createDeleteStatement("id");  
		try {
			con = ConnectionFactory.getConnection(); 
			statement = con.prepareStatement(query);  
			statement.setInt(1, id);
			statement.executeUpdate(); 
			return true;
		} 
		catch(Exception ex) {
			
		} 
		finally {
			ConnectionFactory.close(con); 
			ConnectionFactory.close(statement);
		} 
		return false;
	}
	
	
}
