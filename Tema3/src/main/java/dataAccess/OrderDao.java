package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Orders;
/**
 * This method implements unique methods for the Orders entity.
 * @author Rares
 *
 */
public class OrderDao extends AbstractDao<Orders> {
	
	/**
	 * This method is used to insert a new entry in the database.
	 * @param p The Orders object to be inserted in the database.
	 * @return A boolean true for success , false for failure.
	 */
	 public boolean placeOrder(Orders p) {
		String query = "INSERT INTO orders (customerId , productId , amount , totalPrice ) VALUES ( ? , ? , ? , ?)"; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			statement.setLong(1, p.getCustomerId()); 
			statement.setLong(2, p.getProductId()); 
			statement.setLong(3, p.getAmount()); 
			statement.setLong(4, p.getTotalPrice()); 
			statement.executeUpdate(); 
			return true;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return false;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	}  
	/**
	 * This method returns all the details for the order with the id given as a parameter.
	 * @param id The id of the entry.
	 * @return A String containing the details.
	 */
	public String[] getOrderDetails(int id) {
		String[] output  = new String[7];  
		String query = "SELECT c.name , c.adress , c.email , p.name , p.price , o.totalPrice , o.amount \r\n" + 
				" FROM customer c JOIN orders o JOIN product p ON (p.id = o.productID AND c.id = o.customerId)\r\n" + 
				" WHERE o.id = "+id; 
		Connection con = null; 
		PreparedStatement statement = null; 
		ResultSet rs = null;   
		
		try { 
			
			con = ConnectionFactory.getConnection(); 
			statement = con.prepareStatement(query); 
			rs = statement.executeQuery(); 
			
			rs.next(); 
			for(int i=0;i<7;i++) {
				output[i]=rs.getString(i+1);  
			}
			return output;
			
		} 
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		} 
		finally {
			ConnectionFactory.close(con); 
			ConnectionFactory.close(rs); 
			ConnectionFactory.close(statement);
		} 
		return output;
		
	}
}
