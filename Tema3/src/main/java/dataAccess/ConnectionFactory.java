package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import java.sql.*;
/**
 * This class is used to connect to the database.
 * @author Rares
 *
 */
public class ConnectionFactory {
	@SuppressWarnings("unused")
	private static final Logger LOGGER= Logger.getLogger(ConnectionFactory.class.getName()); 
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver"; 
	private static final String DBURL = "jdbc:mysql://localhost:3306/tema3"; 
	private static final String USER = "root"; 
	private static final String PASS = "rares"; 
	
	private static ConnectionFactory singleInstance = new ConnectionFactory(); 
	/**
	 * This constructor creates a single instance.
	 */
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		} 
		catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	} 
	/**
	 * This method creates the connection to the database.
	 * @return Returns a  Connection Object. 
	 */
	private Connection createConnection() {
		Connection con = null;
		try {
			con = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return con;
	} 
	 
	/**
	 * This method returns the Connection Object.
	 * @return The conncetion to the database.
	 */
	public static Connection getConnection() {
		return singleInstance.createConnection();
	} 
	/**
	 * This method closes the connection to the database.
	 * @param connection Closes the connection based on the parameter.
	 */
	public static void close(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	/**
	 * This method closes the connection to the database.
	 * @param statement Closes the connection based on the parameter.
	 */
	public static void close (Statement statement) {
		try {
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	/**
	 * This method closes the connection to the database.
	 * @param rs Closes the connection based on the parameter.
	 */
	public static void close (ResultSet rs) {
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
