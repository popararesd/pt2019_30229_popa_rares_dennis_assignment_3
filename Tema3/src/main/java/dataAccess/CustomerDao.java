package dataAccess;

import java.sql.*;
import model.Customer;
/**
 * This class implements methods unique to the Customer entity.
 * @author Rares
 *
 */
public class CustomerDao extends AbstractDao<Customer> { 
	
	public boolean insertCustomer(Customer c) {
		String query = "INSERT INTO customer (name , adress , email ) VALUES ( ? , ? , ?)"; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			statement.setString(1, c.getName()); 
			statement.setString(2, c.getAdress()); 
			statement.setString(3, c.getEmail());  
			statement.executeUpdate(); 
			return true;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return false;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	} 
	/**
	 * This method updates the database with the given parameters.
	 * @param id The id of the entry to be modified.
	 * @param name The new name.
	 * @param adress The new adress.
	 * @param email The new email.
	 * @return A boolean true for success , false for failure.
	 */
	public boolean updateCustomer (int id , String name , String adress , String email) {
		String query = "UPDATE customer SET name=?, adress=?, email=? WHERE id="+id; 
		Connection connection = null; 
		PreparedStatement statement = null;  
		try {
			connection = ConnectionFactory.getConnection(); 
			statement = connection.prepareStatement(query);
			statement.setString(1, name); 
			statement.setString(2, adress); 
			statement.setString(3, email);  
			statement.executeUpdate(); 
			return true;
		} 
		catch(Exception ex) { 
			System.out.println(ex.getMessage());
			return false;
		} 
		finally {
			ConnectionFactory.close(connection); 
			ConnectionFactory.close(statement);
		} 
	}
	
}
