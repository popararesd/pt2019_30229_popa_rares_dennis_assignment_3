package presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.*;

import dataAccess.ProductDao;
import model.Comparators;
import model.Product;
import model.SortingFilters;

@SuppressWarnings("serial") 
/**
 * This class represents the Product Window.
 * @author Rares
 *
 */
public class ProductWindow extends JFrame {
	
	private JTable table;  
	private JButton addProduct = new JButton("Add Product"); 
	private JButton editProduct = new JButton("Edit Product"); 
	private JButton deleteProduct = new JButton("Delete Product"); 
	private JButton back = new JButton("BACK");
	protected MenuWindow mw;
	/**
	 * This constructor creates a new instance based on the parameters.
	 * @param table The table containing the entries.
	 * @param mw A Menu Window.
	 */
	public ProductWindow (JTable table , MenuWindow mw) { 
		this.mw = mw;
		this.table = table; 
		this.setSize(600,600); 
		this.setLayout(new GridLayout(2, 1));  
		JScrollPane scroll = new JScrollPane(this.table); 
		this.add(scroll);  
		JPanel panel1 = new JPanel(); 
		panel1.setLayout(new GridLayout(2, 2)); 
		
		JPanel aux1 = new JPanel(); 
		aux1.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux1.add(this.addProduct);  
		
		JPanel aux2 = new JPanel(); 
		aux2.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux2.add(this.editProduct);  
		
		JPanel aux3 = new JPanel(); 
		aux3.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux3.add(this.deleteProduct);  
		
		JPanel aux4 = new JPanel(); 
		aux4.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux4.add(this.back); 
		
		panel1.add(aux1); 
		panel1.add(aux2); 
		panel1.add(aux3); 
		panel1.add(aux4); 
		
		this.add(panel1);  
		this.addProduct.addActionListener(new addClLis()); 
		this.editProduct.addActionListener(new editCl()); 
		this.deleteProduct.addActionListener(new DlCl()); 
		this.back.addActionListener(new ActionListener() {
			
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				ProductWindow.this.hide(); 
				ProductWindow.this.mw.show();
			}
		});
		
	}  
	
	class addClLis implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			addProductWindow aux = new addProductWindow(ProductWindow.this); 
			aux.setVisible(true); 
			ProductWindow.this.hide();
		}
		
	} 
	
	class editCl implements ActionListener {

		@SuppressWarnings({ "deprecation" })
		public void actionPerformed(ActionEvent e) {
			editProductWindow aux = new editProductWindow(ProductWindow.this); 
			aux.setVisible(true); 
			ProductWindow.this.hide();
		}
		
	} 
	
	class DlCl implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			DeleteProduct aux = new DeleteProduct(ProductWindow.this); 
			aux.setVisible(true); 
			ProductWindow.this.hide();
		}
		
	}
	
	public void addClientListener(ActionListener l) {
		this.addProduct.addActionListener(l);
	} 
	
	public void deleteClientListener(ActionListener l) {
		this.deleteProduct.addActionListener(l);
	} 
	
	public void updateClient(ActionListener l) {
		this.editProduct.addActionListener(l);
	}
	
	public void goBack(ActionListener l) {
		this.back.addActionListener(l);
	}
	


} 

@SuppressWarnings("serial")
class addProductWindow extends JFrame {
	
	private JTextField tfName = new JTextField(30);  
	private JTextField tfCategory = new JTextField(30); 
	private JTextField tfPrice = new JTextField(30);  
	private JTextField tfRatings = new JTextField(30);  
	private JTextField tfStock = new JTextField(30); 
	private JButton okBtn = new JButton("Add Product"); 
	private ProductWindow productWinodw = null; 
	
	public addProductWindow(ProductWindow w) {
		this.productWinodw = w; 
		this.productWinodw.setVisible(true);  
		this.setSize(400,400); 
		this.setLayout(new GridLayout(6, 2)); 
		this.add(new JLabel("Name:")); 
		this.add(this.tfName);  
		this.add(new JLabel("Category:")); 
		this.add(this.tfCategory); 
		this.add(new JLabel("Price:")); 
		this.add(this.tfPrice); 
		this.add(new JLabel ("Ratings:")); 
		this.add(this.tfRatings);  
		this.add(new JLabel ("Stock:")); 
		this.add(this.tfStock); 
		this.add(this.okBtn); 
		this.okBtn.addActionListener(new addLis());
	} 
	
	class addLis implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			try {
			ProductDao x = new ProductDao(); 
			int category = Integer.parseInt(addProductWindow.this.tfCategory.getText()); 
			int price = Integer.parseInt(addProductWindow.this.tfPrice.getText()); 
			int ratings = Integer.parseInt(addProductWindow.this.tfRatings.getText()); 
			int stock = Integer.parseInt(addProductWindow.this.tfStock.getText());
			boolean b = x.insertProduct(new Product(0,addProductWindow.this.tfName.getText() , category, price, ratings, stock));
			if(b) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
			}
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			}    
			List<Product> list = x.findAll(); 
			
			SortingFilters filter; 
			if(addProductWindow.this.productWinodw.mw.getState() == 1) {
				filter = SortingFilters.BY_PRICE;
			} 
			else 
				if(addProductWindow.this.productWinodw.mw.getState() == 2) 
					filter = SortingFilters.BY_CATEGORY; 
				else 
					filter = SortingFilters.BY_RATINGS;  
			Collections.sort(list, new Comparators(filter));
			List<Object> objects = new ArrayList<Object>(list); 
			
			JTable table = x.createTable(objects); 
			MenuWindow mw = addProductWindow.this.productWinodw.mw;
			addProductWindow.this.productWinodw = new ProductWindow(table,mw); 
			addProductWindow.this.hide();
			addProductWindow.this.productWinodw.show();
		} 
		catch(Exception ex) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,"ERROR", "Message", JOptionPane.ERROR_MESSAGE);
			
		} 
		finally {
			ProductDao x = new ProductDao();  
			List<Product> list = x.findAll(); 
			
			SortingFilters filter; 
			if(addProductWindow.this.productWinodw.mw.getState() == 1) {
				filter = SortingFilters.BY_PRICE;
			} 
			else 
				if(addProductWindow.this.productWinodw.mw.getState() == 2) 
					filter = SortingFilters.BY_CATEGORY; 
				else 
					filter = SortingFilters.BY_RATINGS;  
			Collections.sort(list, new Comparators(filter));
			List<Object> objects = new ArrayList<Object>(list); 
			
			JTable table = x.createTable(objects); 
			MenuWindow mw = addProductWindow.this.productWinodw.mw;
			addProductWindow.this.productWinodw = new ProductWindow(table,mw); 
			addProductWindow.this.hide();
			addProductWindow.this.productWinodw.show();
		}
		}
		
	}
	
	
} 

@SuppressWarnings("serial")
class editProductWindow extends JFrame {
	
	private JTextField tfId = new JTextField(30);
	private JTextField tfName = new JTextField(30);  
	private JTextField tfCategory = new JTextField(30); 
	private JTextField tfPrice = new JTextField(30);  
	private JTextField tfRatings = new JTextField(30);  
	private JTextField tfStock = new JTextField(30); 
	private JButton okBtn = new JButton("Edit Product"); 
	private ProductWindow productWinodw = null; 
	
	public editProductWindow(ProductWindow w) {
		this.productWinodw = w; 
		this.productWinodw.setVisible(true);  
		this.setSize(400,400); 
		this.setLayout(new GridLayout(7, 2));  
		this.add(new JLabel("Id:")); 
		this.add(this.tfId);
		this.add(new JLabel("Name:")); 
		this.add(this.tfName); 
		this.add(new JLabel("Category:")); 
		this.add(this.tfCategory); 
		this.add(new JLabel ("Price:")); 
		this.add(this.tfPrice); 
		this.add(new JLabel ("Ratings:")); 
		this.add(this.tfRatings);  
		this.add(new JLabel ("Stock:")); 
		this.add(this.tfStock); 
		this.add(this.okBtn); 
		this.okBtn.addActionListener(new editLis());
	}  
	
	class editLis implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			try {
			ProductDao x = new ProductDao();  
			int id = Integer.parseInt(editProductWindow.this.tfId.getText()); 
			int category = Integer.parseInt(editProductWindow.this.tfCategory.getText()); 
			int price = Integer.parseInt(editProductWindow.this.tfPrice.getText()); 
			int ratings = Integer.parseInt(editProductWindow.this.tfRatings.getText()); 
			int stock = Integer.parseInt(editProductWindow.this.tfStock.getText()); 
			String name = editProductWindow.this.tfName.getText();
			boolean b = x.updateProduct(id, name, category, price, ratings, stock);
			if(b) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
			}
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			}    
			
		}  
		catch(Exception ex) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
		
		} 
		finally { 
			ProductDao x = new ProductDao();
			List<Product> list = x.findAll(); 
			
			SortingFilters filter; 
			if(editProductWindow.this.productWinodw.mw.getState() == 1) {
				filter = SortingFilters.BY_PRICE;
			} 
			else 
				if(editProductWindow.this.productWinodw.mw.getState() == 2) 
					filter = SortingFilters.BY_CATEGORY; 
				else 
					filter = SortingFilters.BY_RATINGS;  
			Collections.sort(list, new Comparators(filter));
			List<Object> objects = new ArrayList<Object>(list);
			
			JTable table = x.createTable(objects); 
			MenuWindow mw = editProductWindow.this.productWinodw.mw;
			editProductWindow.this.productWinodw = new ProductWindow(table,mw); 
			editProductWindow.this.hide();
			editProductWindow.this.productWinodw.show();
		}
		}
		
	}
	
} 

@SuppressWarnings("serial")
class DeleteProduct extends JFrame {
	
	private JTextField tfId = new JTextField(30); 
	private ProductWindow productWinodw = null; 
	private JButton okBtn = new JButton("Delete");
	
	public DeleteProduct (ProductWindow w) {
		this.productWinodw = w; 
		this.setSize(150,150); 
		this.setLayout(new GridLayout(2, 2)); 
		JPanel pane = new JPanel(); 
		pane.setLayout(new GridLayout(1, 2)); 
		pane.add(new JLabel("Id :")); 
		pane.add(this.tfId); 
		JPanel pane2 = new JPanel(); 
		pane2.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		pane2.add(this.okBtn); 
		this.add(pane); 
		this.add(pane2); 
		this.okBtn.addActionListener(new delCustList());
		
	} 
	
	class delCustList implements ActionListener{

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) { 
			try {
			ProductDao x = new ProductDao();
			boolean b = x.deleteById(Integer.parseInt(DeleteProduct.this.tfId.getText()));
			if(b) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
			}
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			}    
			
			} 
			catch(Exception ex) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			
			} 
			finally { 
				ProductDao x = new ProductDao();
				List<Product> list = x.findAll(); 
				SortingFilters filter; 
				if(DeleteProduct.this.productWinodw.mw.getState() == 1) {
					filter = SortingFilters.BY_PRICE;
				} 
				else 
					if(DeleteProduct.this.productWinodw.mw.getState() == 2) 
						filter = SortingFilters.BY_CATEGORY; 
					else 
						filter = SortingFilters.BY_RATINGS;  
				Collections.sort(list, new Comparators(filter));
				List<Object> objects = new ArrayList<Object>(list);

				JTable table = x.createTable(objects); 
				MenuWindow mw = DeleteProduct.this.productWinodw.mw;
				DeleteProduct.this.productWinodw = new ProductWindow(table,mw); 
				DeleteProduct.this.hide();
				DeleteProduct.this.productWinodw.show();
			}
		}
		
	}
	
}
