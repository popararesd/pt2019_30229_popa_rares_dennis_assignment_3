package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial") 
/**
 * This class represents the Client Window.
 * @author Rares
 *
 */
public class MenuWindow extends JFrame {
	private JButton clientsBtn = new JButton ("View/Edit Clients"); 
	private JButton productsBtn = new JButton ("View/Edit Products"); 
	private JButton ordersBtn = new JButton("View/Place Orders");  
	private JButton billsBtn = new JButton("Generate Bills"); 
	private static int sortFilter = 1; 
	private static final JComboBox<String> comboBox = new JComboBox<String>(new String[]{"By Price","By Category" , "By Ratings"});
	/**
	 * This constructor creates a new instance of the window.
	 */
	public MenuWindow() { 
		JLabel label = new JLabel("Warehouse Manager"); 
		label.setForeground(Color.YELLOW);
		label.setFont(new Font("Serif", Font.ITALIC, 30)); 
		JPanelWithBackground panel = new JPanelWithBackground("C:\\Users\\Rares\\Desktop\\warehouse 43.jpg");
		panel.add(this.clientsBtn); 
		panel.add(this.productsBtn); 
		panel.add(this.ordersBtn); 
		panel.add(this.billsBtn); 
		panel.add(MenuWindow.comboBox);
		panel.add(label);  
		panel.setLayout(null); 
		panel.setSize(600,400);
		this.add(panel);
		Insets insets = panel.getInsets(); 
		Dimension size = label.getPreferredSize(); 
		label.setBounds(160 + insets.left , 10 + insets.top  , size.width , size.height); 
		size = this.clientsBtn.getPreferredSize(); 
		this.clientsBtn.setBounds(140 + insets.left, 160 + insets.top, size.width, size.height); 
		size = this.productsBtn.getPreferredSize(); 
		this.productsBtn.setBounds(350 + insets.left, 160 + insets.top , size.width, size.height);
		size = this.ordersBtn.getPreferredSize(); 
		this.ordersBtn.setBounds(140 + insets.left, 250 + insets.top, size.width, size.height);
		size = this.billsBtn.getPreferredSize(); 
		this.billsBtn.setBounds(350 + insets.left, 250 + insets.top, size.width, size.height); 
		size = MenuWindow.comboBox.getPreferredSize(); 
		MenuWindow.comboBox.setBounds(350 + insets.left, 200 + insets.top, size.width, size.height);
		this.setLayout(null);
		
		this.setSize(600 + insets.left + insets.right, 400 + insets.top + insets.bottom);
		
		this.setVisible(true);  
		MenuWindow.comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				switch(MenuWindow.comboBox.getSelectedIndex()) {
				case 0: MenuWindow.sortFilter = 1; 
						break; 
				case 1 : MenuWindow.sortFilter = 2; 
						break;  
						
				case 2 : MenuWindow.sortFilter = 3; 
						 break;  
				default : MenuWindow.sortFilter = 1; 
				 		  break;
				}
				
			}
		} );
	} 
	
	
 public int getState() {
	 return MenuWindow.sortFilter;
 }

public void addClientsListener (ActionListener l) {
	this.clientsBtn.addActionListener(l);
} 

public void addProductsListener (ActionListener l) {
	this.productsBtn.addActionListener(l);
} 

public void addOrderListener (ActionListener l) {
	this.ordersBtn.addActionListener(l);
} 

public void addBillListener (ActionListener l) {
	this.billsBtn.addActionListener(l);
}


class JPanelWithBackground extends JPanel {

	  private Image backgroundImage;

	  // Some code to initialize the background image.
	  // Here, we use the constructor to load the image. This
	  // can vary depending on the use case of the panel.
	  public JPanelWithBackground(String fileName)  {
	    try {
			backgroundImage = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	  }

	  public void paintComponent(Graphics g) {
	    super.paintComponent(g);

	    // Draw the background image.
	    g.drawImage(backgroundImage, 0, 0, this);
	  }
	 
} 
}
