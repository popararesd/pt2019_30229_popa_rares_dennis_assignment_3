package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import javax.swing.*;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import dataAccess.OrderDao;


/**
 * This class represents Bill Window.
 * @author Rares
 *
 */

@SuppressWarnings("serial")
public class BillWindow extends JFrame { 
	private JTable table;  
	private JButton generateBill = new JButton("Generate Bill"); 
	private JButton back = new JButton("BACK");  
	private JTextField tfId = new JTextField(20);
	protected MenuWindow mw;
	/**
	 * This constructor creates a new instance based on the parameters.
	 * @param table A JTable containing the orders.
	 * @param mw A MenuWindow.
	 */
	public BillWindow (JTable table , MenuWindow mw) { 
		this.mw = mw; 
		this.table = table; 
		this.setSize(600,600); 
		this.setLayout(new GridLayout(2, 1));  
		JScrollPane scroll = new JScrollPane(this.table); 
		this.add(scroll);
		
		JPanel panel1 = new JPanel(); 
		panel1.setLayout(new GridLayout(2, 2)); 
		
		panel1.add(this.tfId); 
		panel1.add(this.generateBill); 
		panel1.add(this.back);
		this.add(panel1);
		this.generateBill.addActionListener(new GenerateBill());
		 
		this.back.addActionListener(new ActionListener() {
			
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				BillWindow.this.hide(); 
				BillWindow.this.mw.show();
			}
		});
		
	}   
	
	class GenerateBill implements ActionListener {
		
		public void actionPerformed(ActionEvent e) { 
			Document document = new Document();
			try {
			
			PdfWriter.getInstance(document, new FileOutputStream("BillNo"+BillWindow.this.tfId.getText()+".pdf"));
			 
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK); 
			Image img = Image.getInstance("C:\\Users\\Rares\\Desktop\\angrysushi.jpg");
			document.add(img);  
			String[] s = (new OrderDao()).getOrderDetails(Integer.parseInt(BillWindow.this.tfId.getText()));
			String[] headers = {"Name :" , "Adress :" , "Email :" , "Product name :" , "Price : " , "Balance :" , "Amount :"};
			int index = 0; 
			Chunk c = new Chunk("\n\n\n\n\n\n\n");
			document.add(c);
			for(String x : s) { 
			Chunk chunk = new Chunk(headers[index]+x+"\n", font);  
			Paragraph p = new Paragraph(chunk);
			document.add(p); 
			index++;
			}
			} 
			catch(Exception ex) {
				
			} 
			finally {
			document.close(); 
			}
		} 
		
	} 
	
	 
}
