package presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import dataAccess.CustomerDao;
import model.Customer;

@SuppressWarnings("serial")
/**
 * This class represents the Client Window.
 * @author Rares
 *
 */
public class ClientsWindow extends JFrame {
	
	private JTable table;  
	private JButton addClient = new JButton("Add Client"); 
	private JButton editClient = new JButton("Edit Client"); 
	private JButton deleteClient = new JButton("Delete Client"); 
	private JButton back = new JButton("BACK"); 
	protected MenuWindow mw;
	
	/**
	 * This constructor creates a new instance based on the parameters.
	 * @param table The table containing the entries.
	 * @param mw A Menu Window.
	 */
	public ClientsWindow (JTable table , MenuWindow mw) { 
		this.mw = mw;
		this.table = table; 
		this.setSize(600,600); 
		this.setLayout(new GridLayout(2, 1));  
		JScrollPane scroll = new JScrollPane(this.table); 
		this.add(scroll);  
		JPanel panel1 = new JPanel(); 
		panel1.setLayout(new GridLayout(2, 2)); 
		
		JPanel aux1 = new JPanel(); 
		aux1.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux1.add(this.addClient);  
		
		JPanel aux2 = new JPanel(); 
		aux2.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux2.add(this.editClient);  
		
		JPanel aux3 = new JPanel(); 
		aux3.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux3.add(this.deleteClient);  
		
		JPanel aux4 = new JPanel(); 
		aux4.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux4.add(this.back); 
		
		panel1.add(aux1); 
		panel1.add(aux2); 
		panel1.add(aux3); 
		panel1.add(aux4); 
		
		this.add(panel1);  
		this.addClient.addActionListener(new addClLis()); 
		this.editClient.addActionListener(new editCl()); 
		this.deleteClient.addActionListener(new DlCl()); 
		this.back.addActionListener(new ActionListener() {
			
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				ClientsWindow.this.hide(); 
				ClientsWindow.this.mw.show();
			}
		});
		
	}  
	
	class addClLis implements ActionListener {

		@SuppressWarnings({ "deprecation" })
		public void actionPerformed(ActionEvent e) {
			addClientWindow aux = new addClientWindow(ClientsWindow.this); 
			aux.setVisible(true); 
			ClientsWindow.this.hide();
		}
		
	} 
	
	class editCl implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			editClientWindow aux = new editClientWindow(ClientsWindow.this); 
			aux.setVisible(true); 
			ClientsWindow.this.hide();
		}
		
	} 
	
	class DlCl implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			DeleteClient aux = new DeleteClient(ClientsWindow.this); 
			aux.setVisible(true); 
			ClientsWindow.this.hide();
			
		}
		
	}
	
	public void addClientListener(ActionListener l) {
		this.addClient.addActionListener(l);
	} 
	
	public void deleteClientListener(ActionListener l) {
		this.deleteClient.addActionListener(l);
	} 
	
	public void updateClient(ActionListener l) {
		this.editClient.addActionListener(l);
	}
	
	public void goBack(ActionListener l) {
		this.back.addActionListener(l);
	}
	
	/*
	public static void main(String[] args) {
		CustomerDao x = new CustomerDao();  
		List<Customer> list = x.findAll(); 
		List<Object> listTabel = new ArrayList<Object>(list); 
		JTable t = x.createTable(listTabel);  
		
		ClientsWindow w = new ClientsWindow(t); 
		w.setVisible(true);

	}
	*/
} 

@SuppressWarnings("serial")
class addClientWindow extends JFrame {
	
	private JTextField tfName = new JTextField(30);  
	private JTextField tfadress = new JTextField(30); 
	private JTextField tfemail = new JTextField(30); 
	private JButton okBtn = new JButton("Add Customer"); 
	private ClientsWindow clientWinodw = null; 
	
	public addClientWindow(ClientsWindow w) {
		this.clientWinodw = w; 
		this.clientWinodw.setVisible(true);  
		this.setSize(400,400); 
		this.setLayout(new GridLayout(5, 2)); 
		this.add(new JLabel("Name:")); 
		this.add(this.tfName); 
		this.add(new JLabel("Adress:")); 
		this.add(this.tfadress); 
		this.add(new JLabel ("Email:")); 
		this.add(this.tfemail);  
		this.add(this.okBtn); 
		this.okBtn.addActionListener(new addLis());
	} 
	
	class addLis implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			try {
			CustomerDao x = new CustomerDao(); 
			boolean b = x.insertCustomer(new Customer(0,addClientWindow.this.tfName.getText(),addClientWindow.this.tfadress.getText(),addClientWindow.this.tfemail.getText()));
			if(b) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
			}
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			}    
			} 
			catch(Exception ex) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			
			} 
			finally { 
				CustomerDao x = new CustomerDao();
				List<Customer> list = x.findAll(); 
				List<Object> objects = new ArrayList<Object>(list);
				JTable table = x.createTable(objects); 
				MenuWindow mw = addClientWindow.this.clientWinodw.mw;
				addClientWindow.this.clientWinodw = new ClientsWindow(table,mw); 
				addClientWindow.this.hide();
				addClientWindow.this.clientWinodw.show(); 
			}
		}
		
	}
	
	
} 

@SuppressWarnings("serial")
class editClientWindow extends JFrame {
	
	private JTextField tfName = new JTextField(30);  
	private JTextField tfadress = new JTextField(30); 
	private JTextField tfemail = new JTextField(30);  
	private JTextField tfid = new JTextField(30);
	private JButton okBtn = new JButton("Edit Customer Info"); 
	private ClientsWindow clientWinodw = null; 
	
	public editClientWindow(ClientsWindow w) {
		this.clientWinodw = w; 
		this.clientWinodw.setVisible(true);  
		this.setSize(400,400); 
		this.setLayout(new GridLayout(6, 2));  
		this.add(new JLabel("Id:")); 
		this.add(this.tfid);
		this.add(new JLabel("Name:")); 
		this.add(this.tfName); 
		this.add(new JLabel("Adress:")); 
		this.add(this.tfadress); 
		this.add(new JLabel ("Email:")); 
		this.add(this.tfemail);  
		this.add(this.okBtn); 
		this.okBtn.addActionListener(new editLis());
	}  
	
	class editLis implements ActionListener {

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			try {
			CustomerDao x = new CustomerDao(); 
			boolean b = x.updateCustomer(Integer.parseInt(editClientWindow.this.tfid.getText()),editClientWindow.this.tfName.getText(), editClientWindow.this.tfadress.getText(), editClientWindow.this.tfemail.getText());
			if(b) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
			}
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			}    
			} 
			catch(Exception ex) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			
			} 
			finally { 
				CustomerDao x = new CustomerDao();
				List<Customer> list = x.findAll(); 
				List<Object> objects = new ArrayList<Object>(list);
				JTable table = x.createTable(objects); 
				MenuWindow mw = editClientWindow.this.clientWinodw.mw;
				editClientWindow.this.clientWinodw = new ClientsWindow(table,mw); 
				editClientWindow.this.hide();
				editClientWindow.this.clientWinodw.show();
				
			}
		}
		
	}
	
} 

@SuppressWarnings("serial")
class DeleteClient extends JFrame {
	
	private JTextField tfId = new JTextField(30); 
	private ClientsWindow clientWinodw = null; 
	private JButton okBtn = new JButton("Delete");
	
	public DeleteClient (ClientsWindow w) {
		this.clientWinodw = w; 
		this.setSize(150,150); 
		this.setLayout(new GridLayout(2, 2)); 
		JPanel pane = new JPanel(); 
		pane.setLayout(new GridLayout(1, 2)); 
		pane.add(new JLabel("Id :")); 
		pane.add(this.tfId); 
		JPanel pane2 = new JPanel(); 
		pane2.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		pane2.add(this.okBtn); 
		this.add(pane); 
		this.add(pane2); 
		this.okBtn.addActionListener(new delCustList());
		
	} 
	
	class delCustList implements ActionListener{

		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) { 
			try {
			CustomerDao x = new CustomerDao(); 
			boolean b = x.deleteById(Integer.parseInt(DeleteClient.this.tfId.getText()));
			if(b) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
			}
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			}    
			} 
			catch(Exception ex) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
			} 
			finally { 
				CustomerDao x = new CustomerDao();
				List<Customer> list = x.findAll(); 
				List<Object> objects = new ArrayList<Object>(list);
				JTable table = x.createTable(objects); 
				MenuWindow mw = DeleteClient.this.clientWinodw.mw;
				DeleteClient.this.clientWinodw = new ClientsWindow(table,mw); 
				DeleteClient.this.hide();
				DeleteClient.this.clientWinodw.show();
				
			}
		}
		
	}
	
}
