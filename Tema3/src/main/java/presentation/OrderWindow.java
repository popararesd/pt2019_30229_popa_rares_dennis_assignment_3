package presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import dataAccess.OrderDao;
import dataAccess.ProductDao;
import model.Orders;

@SuppressWarnings({ "deprecation", "serial" }) 
/**
 * This class represents the Order Window.
 * @author Rares
 *
 */
public class OrderWindow extends JFrame { 
	private JButton placeOrder = new JButton("Place an order");  
	private JButton back = new JButton("BACK"); 
	protected MenuWindow mw;
	public void goBack(ActionListener l) {
		this.back.addActionListener(l);
	}
	/**
	 * This constructor creates a new instance based on the parameters.
	 * @param table The table containing the entries.
	 * @param mw A Menu Window.
	 */
	public OrderWindow (JTable table , MenuWindow mw) {
		this.mw = mw;
		this.setSize(600,600); 
		this.setLayout(new GridLayout(2, 1));  
		JScrollPane scroll = new JScrollPane(table); 
		this.add(scroll);  
		JPanel panel1 = new JPanel(); 
		panel1.setLayout(new GridLayout(2, 2)); 
		
		JPanel aux1 = new JPanel(); 
		aux1.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux1.add(this.placeOrder);  
		
		JPanel aux2 = new JPanel(); 
		aux2.setLayout(new FlowLayout(FlowLayout.CENTER)); 
		aux2.add(this.back); 
		
		panel1.add(aux1); 
		panel1.add(aux2); 
		this.add(panel1);  
		this.placeOrder.addActionListener(new placeOrderLis()); 
		this.back.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				OrderWindow.this.hide(); 
				OrderWindow.this.mw.show();
			}
		});
	} 
	
	class placeOrderLis implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			PlaceOrderWindow aux = new PlaceOrderWindow(OrderWindow.this); 
			aux.setVisible(true); 
			OrderWindow.this.hide();
			
		}
		
	} 
	
	
} 
@SuppressWarnings({ "deprecation", "serial" })
class PlaceOrderWindow extends JFrame {
	
	private JTextField tfCustomerId = new JTextField(30);  
	private JTextField tfProductId = new JTextField(30); 
	private JTextField tfAmount = new JTextField(30);  
	private JButton okBtn = new JButton("Place Order"); 
	private OrderWindow orderWinodw = null; 
	
	public PlaceOrderWindow(OrderWindow w) {
		this.orderWinodw = w; 
		this.orderWinodw.setVisible(true); 
		this.setSize(400,400); 
		this.setLayout(new GridLayout(6, 2)); 
		this.add(new JLabel("CustomerId:")); 
		this.add(this.tfCustomerId);  
		this.add(new JLabel("ProductId :")); 
		this.add(this.tfProductId); 
		this.add(new JLabel("Amount :")); 
		this.add(this.tfAmount); 
		this.add(this.okBtn); 
		this.okBtn.addActionListener(new PlcOrdLis());
	} 
	
	class PlcOrdLis implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			try {
			OrderDao x = new OrderDao();  
			ProductDao y = new ProductDao();
			int customerId = Integer.parseInt(PlaceOrderWindow.this.tfCustomerId.getText()); 
			int productId = Integer.parseInt(PlaceOrderWindow.this.tfProductId.getText());
			int amount = Integer.parseInt((PlaceOrderWindow.this.tfAmount.getText())); 
			int productPrice = y.getPriceById(productId); 
			int totalPrice = amount * productPrice;  
			int stock = y.getStockById(productId); 
			if(stock>=amount) {
				boolean b = x.placeOrder(new Orders(0, customerId, productId, amount, totalPrice)); 
				if(b) { 
					y.updateStock(productId, stock-amount);
					final JPanel panel = new JPanel();
					JOptionPane.showMessageDialog(panel,"Success", "Message", JOptionPane.OK_OPTION); 
				}
				else {
					final JPanel panel = new JPanel();
					JOptionPane.showMessageDialog(panel,"Error", "Message", JOptionPane.ERROR_MESSAGE);
				}
			} 
			else {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"UnderStock", "Message", JOptionPane.ERROR_MESSAGE);
			}  
			} 
			catch(Exception ex) { 
				
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel,"ERROR", "Message", JOptionPane.ERROR_MESSAGE); 
			} 
			finally {
				OrderDao x = new OrderDao();  
				List<Orders> list = x.findAll(); 
				List<Object> objects = new ArrayList<Object>(list);
				JTable table = x.createTable(objects); 
				MenuWindow mw = PlaceOrderWindow.this.orderWinodw.mw;
				PlaceOrderWindow.this.orderWinodw = new OrderWindow(table,mw); 
				PlaceOrderWindow.this.hide();
				PlaceOrderWindow.this.orderWinodw.show(); 
				
			}
			
		}
		
	}
}
