package model;

/**
 * Class that implements the entity for the customer table from the database.
 * Has the same attributes as the table.
 */

public class Customer {

	private int id;
	private String name;
	private String adress;
	private String email;

	/**
	 * This Constructor creates a new instance of the Object based on the
	 * parameters.
	 * 
	 * @param id     The id of the client.
	 * @param name   The name of the client.
	 * @param adress The adress of the client.
	 * @param email  The email of the client.
	 */

	public Customer(int id, String name, String adress, String email) {
		super();
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.email = email;
	}

	/**
	 * This Constructor creates a new , default , instance of the Object.
	 */
	public Customer() {
		super();
		this.id = 0;
		this.name = "";
		this.adress = "";
		this.email = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
