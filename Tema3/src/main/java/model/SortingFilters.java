package model;
/**
 * This Enumeration contains three possible sorting filters : BY_PRICE , BY_CATEGORY , BY_RATINGS.
 */
public enum SortingFilters {
	BY_PRICE, 
	BY_CATEGORY,
	BY_RATINGS
}
