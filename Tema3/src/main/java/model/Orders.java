package model;

/**
 * This Class implements the entity for the Order table from the DataBase.
 */

public class Orders {  
	private int id; 
	private int customerId;
	private int  productId;  
	private int amount;
	private int totalPrice=0;
 
	
	/**
	 * This Constructor creates a new , default ,  instance of Orders.
	 */	

public Orders() {
		super();
		this.id=0; 
		this.customerId=0; 
		this.productId=0; 
		this.totalPrice=0; 
		this.amount = 0;
} 
/**
 * This Constructor creates a new instance of Orders based on the parameters. 
 * @param id The id of the order. 
 * @param customerid The id of the customer. 
 * @param productid The id of the product. 
 * @param amount The number of products that were ordered. 
 * @param totalPrice The total price of the order.
 */
public Orders(int id , int customerid , int productid ,int amount, int totalPrice) {
	super(); 
	this.id=id; 
	this.customerId = customerid; 
	this.productId = productid; 
	this.totalPrice = totalPrice;  
	this.amount = amount;
	
	
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getCustomerId() {
	return customerId;
}

public void setCustomerId(int customerId) {
	this.customerId = customerId;
}

public int getProductId() {
	return productId;
}

public void setProductId(int productId) {
	this.productId = productId;
}

public int getAmount() {
	return amount;
}

public void setAmount(int amount) {
	this.amount = amount;
}

public int getTotalPrice() {
	return totalPrice;
}

public void setTotalPrice(int totalPrice) {
	this.totalPrice = totalPrice;
}



}