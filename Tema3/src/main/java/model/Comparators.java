package model;

import java.util.Comparator;

/**
 * Used to filter the products stored in a Collection. Implements the comparable
 * interface.
 */
public class Comparators implements Comparator<Product> {

	private SortingFilters filter;

	/**
	 * Constructor that creates a new instance based on the parameter. It returns 0
	 * if equal , -1 if the second is greater than the first or 1 if the first is
	 * greater.
	 * 
	 * @param filter A SortingFilters Object to determine which sorting order to be
	 *               used.
	 */

	public Comparators(SortingFilters filter) {
		super();
		this.filter = filter;
	}

	/**
	 * This method returns an integer based on the the parameters and the
	 * SortingFilter. It returns 0 if equal , -1 if the second is greater than the
	 * first or 1 if the first is greater.
	 * 
	 * @param o1 A Product Object.
	 * @param o2 A Product Object
	 * @return An integer.
	 */
	public int compare(Product o1, Product o2) {
		if (this.filter == SortingFilters.BY_CATEGORY)
			return o1.getCategory() - o2.getCategory();
		if (this.filter == SortingFilters.BY_PRICE)
			return o1.getPrice() - o2.getPrice();
		return o1.getRatings() - o2.getRatings();
	}

}
