package model;

import java.util.ArrayList;
		/**
		 * This Class implements the entity for the Order table from the DataBase. 
		 * The class' attributes match those of the database.
		 */

public class Product {

	private int id;
	private String name;
	private int category;
	private int price;
	private int ratings;
	private int stock;

	/**
	 * This Constructor creates a new instance of Product based on the parameters.
	 * 
	 * @param id       The id of the product.
	 * @param name     The name of the product.
	 * @param category The category of the product.
	 * @param price    The price of the product.
	 * @param ratings  The ratings of the product.
	 * @param stock    The stock of the product aka the number of products in stock.
	 */
	public Product(int id, String name, int category, int price, int ratings, int stock) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.price = price < 0 ? -price : price;
		this.ratings = ratings % 11;
		this.stock = stock;
	}

	/**
	 * This Constructor creates a new , default , instance of Product.
	 */
	public Product() {
		super();
		this.id = 0;
		this.name = "";
		this.category = 0;
		this.price = 0;
		this.ratings = 0;
		this.stock = 0;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getRatings() {
		return ratings;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	public String toString() {
		return "ID=" + this.id;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * This method converts an ArrayList(Product) to a String.
	 * 
	 * @param list The ArrayList(Product) that is to be converted.
	 * @return A String that contains the info about the products.
	 */
	public static String toString(ArrayList<Product> list) {
		String out = "";
		for (Product p : list)
			out += p.toString();
		return out;
	}

}
